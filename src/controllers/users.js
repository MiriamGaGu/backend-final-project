const User = require('../models/User')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')


const index = (req, res, next) => {
  // console.log(res.locals);
  User
    .find().exec()
    .then((users) => 
    res
    .status(200)
    .json({
       users, 
       total: users.length 
    }))
    .catch(e => res.status(500).json({ msg: "Server error", error: e }))
}

const signup = (req, res) => {
  User
    .find({ email: req.body.email })
    .exec()
    .then(users => {
      if (users.length < 1) {
        bcrypt.hash(req.body.password, 10, (err, hash) => {
          if (err) {
            res.status(500)
              .json({ message: err })
          }

          const newUser = new User({
            _id: new mongoose.Types.ObjectId(),
            name: req.body.name,
            email: req.body.email, 
            password: hash
          })
          newUser
            .save()
            .then(saved => {
              res.status(200)
                .json({
                  message: "User created successfully",
                  data: saved
                })
            })
        })
      } else {
        res.status(422) // ya existe ese obj
          .json({
            message: 'User already exist.'
          })
      }
    })
}

const findBy = (req, res) => {
  User.findById(req.params.userId).exec()
    .then(data => res.status(200).json({ type: 'Get User by Id', data: data }))
    .catch(e => res.status(500).json(e))
}


const updateBy = ({ body, params }, res) => {

  User.findOne({ _id: params.userId })
    .then(user => {

      bcrypt.compare(body.password, user.password, (error, result) => {
        if (error) {
          return res.status(401).json({ message: "Authnetication Failed" })
        }

        if (result) {
          user.name = body.name

          user.save().then(updated => res.status(201).json({ message: "User Updated successfully", user: updated }))

        } else {
          bcrypt.hash(body.password, 10, (error, hash) => {
            if (error) {
              res.status(500).json({ message: error })
            }

            user.email = body.email
            user.password = hash

            user.save()
              .then(updated => res.status(200).json({ "Password Updated": updated }))
              .catch(e => res.status(500).json(e))
          })
        }
      })
    })
    .catch(err => {
      console.log(`caught the error: ${err}`);
      res.status(404).json({ "type": "Not Found.", error, err })
    })

}


const login = (req, res) => {
  User.find({ email: req.body.email }).exec()
    .then(user => {
      if (user.length > 0) {
        //comparing passwords

        bcrypt.compare(req.body.password, user[0].password, (error, result) => {
          if (error) {
            return res.status(401).json({ message: "Authnetication Failed" })
          }
          // if not, create token

          console.log(`Role is ${user[0].role}`);

          if (result) {
            const token = jwt.sign({
              name: user[0].name,
              email: user[0].email
            }, process.env.JWT_SECRETKEY, {
                expiresIn: "1hr"
              });

            return res.status(200).json({
              message: "Authentication Successful",
              token
            })
          }
          res.status(401).json({ message: "Authnetication Failed" })


        })
      } else {
        res.status(422).json({ message: "Authnetication Failed" })
      }

    })
}

const deleteBy = (req, res) => {
  User
    .findById(req.params.userId, function (err, user) {
      if (!err) {
        User.deleteMany({ user: { $in: [user._id] } }, function (err) { })
        user
          .remove()
          .then(() => {
            res.status(200)
              .json({
                message: 'User was deleted'
              })
          })
      }

    }).catch(err => {
      console.log(`caugth err: ${err}`);
      return res.status(500).json({ message: 'You do not have permission' })
    })

}

//Admin role
// const isAdmin = (req, res, next) => {
//   const token = req.headers.authorization.split(' ')[1]
//   const email = jwt.verify(token, process.env.JWT_SECRETKEY).email

//   User.find({ email }).exec()
//     .then(user => {

//       console.log(user[0].admin);

//       if (typeof user[0].admin !== "undefined") {
//         // const admin = typeof user[0].admin
//         //res.locals.admin = admin  // Passing info by next
//         next()
//       } else {
//         res.status(401).json({ msg: "You do not have permission" })
//       }
//     })
//     .catch(e => res.status(500).json({ msg: "Server error", error: e }))
// }



module.exports = {
  index,
  findBy,
  updateBy,
  signup,
  login,
  deleteBy
  // isAdmin

}