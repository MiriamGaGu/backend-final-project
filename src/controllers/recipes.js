const User = require('../models/User')
const Recipe = require('../models/Recipe')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const mongoose = require('mongoose')

const index = (req, res, next) => {
    // console.log(res.locals);
    Recipe
        .find().exec()
        .then((users) =>
            res
                .status(200)
                .json({
                    users,
                    total: users.length
                }))
        .catch(e => res.status(500).json({ msg: "Server error", error: e }))
}

const create = (req, res) => {
    const newRecipe = new Recipe({
        _id: mongoose.Types.ObjectId(),
        likes: req.body.likes,
        description: req.body.description,
        photo: req.body.photo,
        user: req.body.userId
    })
    newRecipe
        .save()
        .then(data => {
            res.json({
                type: 'New Recipe',
                data: data
            })
                .status(200)

        })
        .catch(err => {
            console.log(`caugth err ${err}`);
            return res.status(500).json({ message: 'Post Failed' })
        })

}


const findBy = (req, res) => {
    Recipe.findById(req.params.recipeId).exec()
        .then(data => res.status(200).json({ type: 'Get Recipe by Id', data: data }))
        .catch(e => res.status(500).json(e))
}


const updateBy = ({ body, params }, res) => {

    Recipe.findOne({ _id: params.recipeId })
        .then(recipe => {

            recipe.likes = body.likes,
                recipe.description = body.description,
                recipe.photo = body.photo,


                recipe.save()
                    .then(updated => res.status(200).json({ "Recipe Updated": updated }))
                    .catch(e => res.status(500).json(e))

        })
        .catch(err => {
            console.log(`caught the error: ${err}`);
            res.status(404).json({ "type": "Not Found.", error, err })
        })

}

const deleteBy = (req, res) => {
    Recipe
        .findById(req.params.recipeId, function (err, recipe) {
            if (!err) {
                Recipe.deleteMany({ recipe: { $in: [recipe._id] } }, function (err) { })
                recipe
                    .remove()
                    .then(() => {
                        res.status(200)
                            .json({
                                message: 'Recipe was deleted'
                            })
                    })
            }

        }).catch(err => {
            console.log(`caugth err: ${err}`);
            return res.status(500).json({ message: 'You do not have permission' })
        })

}




module.exports = {
    index,
    findBy,
    updateBy,
    create,
    deleteBy

}