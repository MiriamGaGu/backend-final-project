const { Router } = require('express');
const app = Router();

//require Auth
const isAuthenticated = require('../../services/Auth');
const Users = require('../controllers/users');
const Recipes = require('../controllers/recipes')

//Route User
app.route('/users')
  .get(isAuthenticated, Users.index);

app.route('/users/:userId')
  .get(isAuthenticated, Users.findBy)
  .put(isAuthenticated, Users.updateBy)
  .delete(isAuthenticated, Users.deleteBy)

//Route aute
app.post('/auth/signup', Users.signup);
app.post('/auth/login', Users.login);

//Recipe
app.route('/recipes')
  .get(isAuthenticated, Recipes.index)
  .post(isAuthenticated, Recipes.create)

app.route('/recipes/:recipeId')
  .get(isAuthenticated, Recipes.findBy)
  .put(isAuthenticated, Recipes.updateBy)
  .delete(isAuthenticated, Recipes.deleteBy)


module.exports = app

// GET	~/api/v1/recipes	Todas las recetas
// GET	~/api/v1/recipes/:recipieId/photos	Foto de una receta en particular
// PUT	~/api/v1/recipes/:recipieId	Actualizar una receta en particular
// DELETE	~/api/v1/recipes/:recipietId	Borrar una receta en particular
// PUT	~/api/v1/recipes/:recipieId/photos	Actualizar la foto de una receta en particular
// DELETE	~/api/v1/recipes/:recipieId/photos	Borrar la foto de une receta en particular