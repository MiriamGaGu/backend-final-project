const mongoose = require('mongoose');
const { Schema } = mongoose;

const recipeSchema = new Schema({
    _id: mongoose.Schema.Types.ObjectId,
    likes: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        unique: true
    },
    photo: {
        type: String,
        required: true
    },
    user: { type: Schema.Types.ObjectId, ref: 'User' },



})

module.exports = mongoose.model('Recipe', recipeSchema)